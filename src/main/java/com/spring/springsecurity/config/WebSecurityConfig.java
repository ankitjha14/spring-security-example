package com.spring.springsecurity.config;

import com.spring.springsecurity.exceptionHandler.RestAccessDeniedHandler;
import com.spring.springsecurity.exceptionHandler.RestAuthenticationEntryPoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("john_doe")
                .password("{noop}student_password")
                .authorities("ROLE_STUDENT_USER")
                .and()
                .withUser("jane_doe")
                .password("{noop}admin_password")
                .authorities("ROLE_OFFICE_ADMIN");
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf()
                .disable()
                .exceptionHandling()
                .accessDeniedHandler(accessDeniedHandler());

        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, "/course").hasAuthority("ROLE_OFFICE_ADMIN")
                .antMatchers(HttpMethod.POST, "/student").hasAnyAuthority("ROLE_OFFICE_ADMIN", "ROLE_STUDENT_USER")
                .antMatchers(HttpMethod.GET, "/course").permitAll()
                .antMatchers(HttpMethod.GET, "/test").hasAuthority("ROLE_STUDENT_USER")
                .and()
                .httpBasic()
                .authenticationEntryPoint(authenticationEntryPoint());

    }

    @Bean
    RestAccessDeniedHandler accessDeniedHandler() {
        return new RestAccessDeniedHandler();
    }

    @Bean
    RestAuthenticationEntryPoint authenticationEntryPoint() {
        return new RestAuthenticationEntryPoint();
    }
}
